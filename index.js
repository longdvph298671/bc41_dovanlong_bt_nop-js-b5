
/**
 * bài 1
 */
function diemKhuVuc(khuVuc){
    if(khuVuc == "A"){
        return 2;
    }
    else if(khuVuc == "B"){
        return 1;
    }
    else if(khuVuc == "C") {
        return 0.5;
    }
    else {
        return 0;
    }
}
function diemDoiTuong(doiTuong){
    if(doiTuong == "1"){
        return 2.5;
    }
    else if(doiTuong == "2"){
        return 1.5;
    }
    else if(doiTuong == "3") {
        return 1;
    }
    else {
        return 0;
    }
}

function tinhDiem() {
    var diemChuan = document.getElementById("diemChuan").value*1;
    var diemMon1 = document.getElementById("diemMon1").value*1;
    var diemMon2 = document.getElementById("diemMon2").value*1;
    var diemMon3 = document.getElementById("diemMon3").value*1;
    var khuVuc = document.getElementById("khuVuc").value;
    var doiTuong = document.getElementById("doiTuong").value;

    var tongDiem = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc(khuVuc) + diemDoiTuong(doiTuong);
    if(diemMon1 <= 0 || diemMon2 <= 0 || diemMon3 <= 0) {
        result.innerHTML = `<h3>Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0</h3>`;

    }
    else {
        if(tongDiem >= diemChuan) {
            result.innerHTML = `<h3>Bạn đã đậu. Tổng điểm: ${tongDiem}</h3>`;
        }
        else {
            result.innerHTML = `<h3>Bạn đã rớt. Tổng điểm: ${tongDiem}</h3>`;
        }
    }

}



/**
 * bài 2
 */
function tinhTienDien(){
    var hoTen =  document.getElementById("ho-ten").value;
    console.log("🚀 ~ file: index.js:88 ~ tinhTienDien ~ hoTen", hoTen)
    var soKW =  document.getElementById("soKW").value*1;
    console.log("🚀 ~ file: index.js:89 ~ tinhTienDien ~ soKW", soKW)
    var tongTienDien = 0;
    if(soKW <= 0){
        alert("Số kw không hợp lệ! Vui lòng nhập lại");
    }
    else {
        if(soKW <= 50){
            tongTienDien = soKW * 500;
        }
        else if(soKW <= 100){
            tongTienDien = 50 * 500 + (soKW-50) * 650;
        }
        else if(soKW <= 200){
            tongTienDien = 50*500 + 50*650 + (soKW-100) * 850;
        }
        else if(soKW <= 350){
            tongTienDien = 50*500 + 50*650 + 100*850 + (soKW-200) * 1100;
        }
        else {
            tongTienDien = 50*500 + 50*650 + 100*850 + 150*1100 + (soKW-350) * 1300;
        }
    }
    result.innerHTML = `<h3>Họ tên: ${hoTen}; Tiền điện: ${new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(tongTienDien)}</h3>`;
}
